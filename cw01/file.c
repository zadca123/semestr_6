#include <p24fj128ga010.h>

// Konfiguracja dla Explorer 16 z progr. icd2
_CONFIG1(JTAGEN_OFF &GCP_OFF &GWRP_OFF &BKBUG_OFF &COE_OFF &FWDTEN_OFF)
  _CONFIG2(FCKSM_CSDCMD &OSCIOFNC_ON &POSCMOD_HS &FNOSC_PRI)

#define SCALE 308L

int main(void) {
  unsigned long i;
  unsigned char display = 0;

  // zmienne do BCD
  int bcd = 0;

  // zmienne do wezyka
  int wezyk = 7;
  int kierunek = 0;

  // zmienne do kolejki
  int limit = 256;
  int kolejka = 0;

  // zmienne do prng
  int xored, S = 1;

  // rozne
  int przelacznik = 0;
  int x = 1;

  // inicjalizacja
  PORTA = 0x0000;
  TRISA = 0xFF00;
  TRISD = 0xFFFF;

  // nieko�?cz�?ca si�? p�?tla
 again:
  Nop();
  PORTA = (unsigned int)display;
  for (i = 500L * SCALE; i > 0; i--)
    Nop();
  if (PORTDbits.RD13 == 0)
    przelacznik--;
  else if (PORTDbits.RD6 == 0)
    przelacznik++;
  else {
    przelacznik = przelacznik % 9;
    // display=0;
    switch (przelacznik) {
    case 0:
      display = x;
      x++;
      break;

    case 1:
      display = x;
      x--;
      break;

    case 2:
      display = x ^ (x >> 1);
      x++;
      break;

    case 3:
      display = x ^ (x >> 1);
      x--;
      break;

    case 4:
      if(bcd > 99)
	bcd=0;
      display = (bcd / 10 << 4) | (bcd % 10);
      bcd++;
      break;

    case 5:
      if (bcd < 0)
	bcd = 99;
      display = (bcd / 10 << 4) | (bcd % 10);
      bcd--;
      break;

    case 6:
      display = wezyk;
      if (kierunek == 0) {
	wezyk = wezyk << 1;
	if (wezyk >= 224)
	  kierunek = 1;
      } else {
	wezyk = wezyk >> 1;
	if (wezyk <= 7)
	  kierunek = 0;
      }
      break;

    case 7:
      display = kolejka;
      kolejka += x - (x >> 1);
      x = x << 1;
      
      if(kolejka > 256){
        kolejka = 0;
        limit = 256;
        x = 1;	
      } else if(x == limit) {
	limit = limit >> 1;
	x = 1;
      }
      break;

    case 8:
      xored = ((S >> 0)^(S >> 1)^(S >> 4)^(S >> 5)) & 1;
      S = S >> 1;
      S = (S & ~ (1 << 5)) | (xored << 5);
      display = S;
      break;
    }
  }

  goto again;
}
