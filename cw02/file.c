#include <p24fj128ga010.h>

// Konfiguracja dla Explorer 16 z progr. icd2
_CONFIG1(JTAGEN_OFF &GCP_OFF &GWRP_OFF &BKBUG_OFF &COE_OFF &FWDTEN_OFF)
_CONFIG2(FCKSM_CSDCMD &OSCIOFNC_ON &POSCMOD_HS &FNOSC_PRI)

#define SCALE 308L
#define TSENS 4
#define AINPUTS 0xffcf

void ADCinit(int amask) {
  AD1PCFG = amask;
  AD1CON1 = 0x00e0;
  AD1CSSL = 0;
  AD1CON2 = 0;
  AD1CON3 = 0x1f02;
  AD1CON1bits.ADON = 1;
}

int readADC(int ch) {
  AD1CHS = ch;
  AD1CON1bits.SAMP = 1;
  while (!AD1CON1bits.DONE)
    ;
  return ADC1BUF0;
}

int main(void) {
  int limit = 250; // musi byc przed zmienna temp
  unsigned long i;
  unsigned char display = 0;
  int temp = limit - 10;        // wartosc startowa inaczej moze cos nie pyknac
  unsigned int szybkosc = 100L; // z ta szybkoscia lepiej widac miganie diody

  // zmienne pomocnicze
  int poczekaj = 30 * szybkosc;
  int counter = 0;
  int przelacznik = 0;

  // inicjalizacja
  PORTA = 0x0000;
  TRISA = 0xFF00;
  TRISD = 0xFFFF;
  ADCinit(AINPUTS); /*inicjalizacja konwertera AD*/

  while (1) {
    Nop();
    PORTA = (unsigned int)display;
    for (i = szybkosc * SCALE; i > 0; i--)
      Nop();

    if (przelacznik == 1 && temp < limit)
      display = 0;
    else if (przelacznik == 0 && temp >= limit) {
      if (counter >= poczekaj) {
        display = 255;
        przelacznik = 1;
      } else {
        display = !display;
        counter += szybkosc;
        continue;
      }
    } else if (PORTDbits.RD6 == 0) {
      display = 0;
      przelacznik = 1;
    }

    temp = readADC(TSENS);
  }
}
