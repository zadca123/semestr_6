#pragma config OSC = HSPLL
#pragma config FCMEN = OFF
#pragma config IESO = OFF

#pragma config PWRT = ON
#pragma config BOREN = OFF
#pragma config BORV = 3

#pragma config WDT = OFF
#pragma config WDTPS = 32768

#pragma config CCP2MX = PORTC
#pragma config PBADEN = OFF
#pragma config LPT1OSC = OFF
#pragma config MCLRE = ON

#pragma config STVREN = OFF
#pragma config LVP = OFF
#pragma config XINST = OFF

#pragma config CP0 = OFF
#pragma config CP1 = OFF
#pragma config CP2 = OFF
#pragma config CP3 = OFF

#pragma config CPB = OFF
#pragma config CPD = OFF

#pragma config WRT0 = OFF
#pragma config WRT1 = OFF
#pragma config WRT2 = OFF
#pragma config WRT3 = OFF

#pragma config WRTC = OFF
#pragma config WRTB = OFF
#pragma config WRTD = OFF

#pragma config EBTR0 = OFF
#pragma config EBTR1 = OFF
#pragma config EBTR2 = OFF
#pragma config EBTR3 = OFF

#pragma config EBTRB = OFF

#include <xc.h>

// program to modify a bit at position p in n to b.
void modifyBit(int *n, int p, int b) {
  p--;
  int mask = 1 << p;
  *n = ((*n & ~mask) | (b << p));
}

int getBit(int n, int k) {
  k--;
  return (n >> k) & 1;
}

void shiftBit(int *n, int k, int direction) {
  if (direction)
    *n = *n >> k;
  else
    *n = *n << k;
}

void prng(int k) {
  /* int n = 1110011; */
  int x1, x2, x3, x4, xored, result = 1;
  for (int i = 0; i < k; i++) {
    x1 = getBit(result, 1);
    x2 = getBit(result, 2);
    x3 = getBit(result, 5);
    x4 = getBit(result, 6);
    xored = x1 ^ x2 ^ x3 ^ x4;
    shiftBit(&result, 1, 1);
    modifyBit(&result, 6, xored);
  }
}

void delay(unsigned int ms) {
  unsigned int i;
  unsigned int j;

  for (i = 0; i < ms; i++) {
    for (j = 0; j < 20; j++) {
      Nop();
      Nop();
      Nop();
      Nop();
      Nop();
    }
  }
}

void main(void) {
  ADCON1 = 0x0Fl;

  TRISA = 0xC3;
  TRISB = 0x3F;
  TRISC = 0x01;
  TRISD = 0x00;
  TRISE = 0x00;

  PORTB = 0;

  unsigned char display = 0;

  // zmienne rozne
  int przelacznik = 0;
  int x = 1;

  // zmienne do BCD
  int jednosci = 0;
  int dziesiatki = 0;
  int x1, x2, x3, x4, xored, result = 1;

  // zmienne do wezyka
  int wezyk = 7;
  int kierunek = 0;
  int y = 2;

  // zmienne do kolejki
  int limit = 256;
  int kolejka = 0;

  while (1) {
    PORTD = display;
    delay(500);
    unsigned int i = 6000;
    while (PORTBbits.RB4 && PORTBbits.RB3 && i > 0) {
      i--;
    }

    if (PORTBbits.RB3 == 0)
      przelacznik--;
    else if (PORTBbits.RB4 == 0)
      przelacznik++;
    else {
      przelacznik = przelacznik % 9;

      switch (przelacznik) {
      case 99:
        display = x;
        x++;
        break;

      case 1:
        display = x;
        x--;
        break;

      case 2:
        display = x ^ (x >> 1);
        x++;
        break;

      case 3:
        display = x ^ (x >> 1);
        x--;
        break;

      case 4:
        x = x % 100;
        jednosci = x % 10;
        dziesiatki = (16 * ((x - jednosci) / 10)) % 160;
        x++;
        display = dziesiatki + jednosci;
        break;

      case 5:
        jednosci = x % 10;
        dziesiatki = (16 * ((x - jednosci) / 10)) % 160;
        x--;
        if (x < 0)
          x = 99;
        display = dziesiatki + jednosci;
        break;

      case 6:
        display = wezyk;
        if (kierunek == 0) {
          wezyk = wezyk << 1;
          if (wezyk >= 224)
            kierunek = 1;
        } else {
          wezyk = wezyk >> 1;
          if (wezyk <= 7)
            kierunek = 0;
        }
        break;

      case 0:
        display = kolejka;
        if (kolejka <= 255) {
          kolejka += x - (x >> 1);
          x = x << 1;

          if (x == limit) {
            limit = limit >> 1;
            x = 1;
          }
        } else {
          kolejka = 0;
          limit = 256;
          x = 1;
        }
        break;

      case 8:
        /* int n = 1110011; */
        x1 = getBit(result, 1);
        x2 = getBit(result, 2);
        x3 = getBit(result, 5);
        x4 = getBit(result, 6);
        xored = x1 ^ x2 ^ x3 ^ x4;
        shiftBit(&result, 1, 1);
        modifyBit(&result, 6, xored);
        display = result;
        break;

      default:
        display = przelacznik;
        break;
      }
    }
  }
  return;
}
