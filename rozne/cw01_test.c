#include <stdio.h>  // for printf?
#include <unistd.h> // for sleep command

void binaryQueue() {
  int kolejka = 0;
  int x = 1;
  int limit = 256;

  while (kolejka <= 255) {
    printf("%d\n", kolejka);
    sleep(1);
    kolejka += x - (x >> 1);
    x = x << 1;

    if (x == limit) {
      limit = (limit >> 1);
      x = 1;
    }
  }
  // // if we want infinite loop
  // else {
  //   kolejka = 0;
  //   x = 1;
  //   limit = 256;
  // }
}
void foo2() {
  int max = 99;
  int min = 0;
  int wynik = 0;
  while (min < 99){
    printf("%d\n", wynik);
    sleep(1);
    wynik = (min / 10 << 4) | (min % 10);
    min++;
  }
}

void prng2(int k){
  int xored, S = 1;
  for(int i =0;i<k;i++){
    xored = (((S >> 0)^(S >> 1)^(S >> 4)^(S >> 5)) & 1);
    S = (S >> 1);
    S = ((S & ~ (1 << 5)) | (xored << 5));
    printf("%d ", S);
  }
}

void bcdminus_old(){
  int dziesiatki,jednosci,wynik, min = 0;
  for(int x=99;x>min;x--){
    x = x % 100;
    jednosci = x % 10;
    dziesiatki = (16 * ((x - jednosci) / 10)) % 160;
    wynik = dziesiatki + jednosci;
    printf("%d\n", wynik);
  }
}

void bcdplus_old(){
  int x=0,dziesiatki,jednosci, wynik;
  
  while(1){
    x = x % 100;
    jednosci = x % 10;
    dziesiatki = (16 * ((x - jednosci) / 10)) % 160;
    x++;
    wynik = dziesiatki + jednosci;
    printf("%d\n", wynik);
    sleep(1);
  }
} 
  
  


void bcdplus(){
  int result, max = 99;
  for(int i=0;i<max;i++){
    result = (i/10 << 4) | (i%10);
    printf("%d\n", result);
  }
}

void bcdminus(){
  int result, min = 0;
  for(int i=99;i>min;i--){
    result = (i/10 << 4) | (i%10);
    printf("%d\n", result);
  }
}


void random(){
  int S = 1;
  int xored = 0;
  int wynik = 0;
  int x = 3;
  while(1){
    xored = (((S >> 0) ^ (S >> 1) ^ (S >> 4) ^ (S >> 5)) & 1);
    wynik = (S >> 1) / (S << 5);
    S = wynik;
    printf("%d\n", S);
    sleep(1);
  }
}

int main() {

  /* binaryQueue(); */
  /* foo2(); */
  /* random(); */
  /* prng(100); */
  
  /* bcdplus(); */
  /* bcdplus_old(); */
  
  bcdminus_old();
  bcdminus();
  printf("Hello World\n");

  return 0;
}

