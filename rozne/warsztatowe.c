#include <stdio.h>

// program modyfikuje bit zmiennej n, na pozycji p na wartość b.
void modifyBit(int *n, int p, int b) {
  int mask = 1 << p;
  *n = ((*n & ~mask) | (b << p));
}

// wypisuje wpisaną cyfrę dziesiętną na binarna
void decToBin(int n) {
  for (int i = 31; i >= 0; i--) {
    int k = n >> i;
    if (k & 1)
      printf("%d", 1);
    else
      printf("%d", 0);
  }
  printf("\n");
}

// zwroc k-ty bit zmiennej n
int getBit(int n, int k) { return (n >> k) & 1; }

// przesun o k bitów zmienną n w podanym kierunku
void shiftBit(int *n, int k, int direction) {
  if (direction)
    *n = *n >> k;
  else
    *n = *n << k;
}

// generator liczb pseudolosowych z wykorzystaniem powyższych funkcji
void prng(int k) {
  /* int n = 1110011; */
  int x1, x2, x3, x4, xored, result = 1;
  for (int i = 0; i < k; i++) {
    x1 = getBit(result, 0);
    x2 = getBit(result, 1);
    x3 = getBit(result, 4);
    x4 = getBit(result, 5);
    xored = x1 ^ x2 ^ x3 ^ x4;
    /* xored = ((result >> 0) ^ (result >> 1) ^ (result >> 4) ^ (result >> 5)) & 1; */
    shiftBit(&result, 1, 1);
    modifyBit(&result, 5, xored);
    printf("%d\n", result);
  }
}

int main() {
  // wypisz 1000 pseudolosowych liczb
  prng(1000);
  //wypisz liczbę dziesiętną 127 jako binarną
  decToBin(127);
  return 0;
}
