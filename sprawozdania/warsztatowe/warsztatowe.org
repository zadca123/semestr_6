#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt]
#+LaTeX_HEADER: \usepackage[letterpaper,margin=1in, top=.75in, bottom=.75in]{geometry}
#+LaTeX_HEADER: \usepackage{titletoc}
#+LaTeX_HEADER: \usepackage{wrapfig}
#+LaTeX_HEADER: \usepackage[export]{adjustbox}
#+OPTIONS: toc:nil H:7 ^:nil todo:nil
#+TITLE: Zadania warsztatowe
#+AUTHOR: Adam Salwowski


* COMMENT Spis treści                                                           :toc:
- [[#źródła-wiedzy][Źródła wiedzy]]
- [[#zmienne][Zmienne]]
- [[#funkcje][Funkcje]]
  - [[#working-modifybit][WORKING =modifyBit=]]
  - [[#dectobin][=decToBin=]]
  - [[#getbit][=getBit=]]
  - [[#shiftbit][=shiftBit=]]
  - [[#prng][=prng=]]
- [[#main][=main=]]

* DONE Funkcje
** TODO =modifyBit=
#+CAPTION: Zmodyfikuj k-ty bit zmiennej
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 3 :lines "3-8"
Funkcja przyjmuje referencje na zmienną =n=.
** DONE =decToBin=
CLOSED: [2022-05-20 Fri 22:23]
#+CAPTION: Wypisz wartość binarną wartości dziesiętnej zmiennej =n=
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 9 :lines "9-20"
W pętli, zmienna =n= jest przesównana bitowo w prawo o =i=. Jeśli wartość na bicie wynosi =1=, wypisz =1=, jeśli nie to =0=.
** DONE =getBit=
CLOSED: [2022-05-14 Sat 17:08]
#+CAPTION: Zwróć bit zmiennej n na k-tym miejscu
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 21 :lines "21-23"
Zmienną =n= bitowo przesówamy w prawo o =k= miejsc, a wynik poddajemy operacji binarnego AND przez =1=. Jeśli przesunięcie zwróci =1=, zwróć =1=.

** DONE =shiftBit=
CLOSED: [2022-05-20 Fri 22:45]
#+CAPTION: Przesunięcie bitowe
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 24 :lines "24-31"
Funkcja przeprowadza operację przesunięcia bitowego na zmiennej =n=. Podajemy referecję do zmiennej =n= jako parametr. =k= jest ilością przesunięcia a =direction= kierunkiem.
** DONE =prng=
CLOSED: [2022-05-14 Sat 17:28]
#+CAPTION: Generator liczb pseudo-losowych
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 32 :lines "32-47"
W tej funkcji wykorzystuję poprzednie funkcje do stworzenia generatora. =result = 1;= jest ziarnem początkowym.
* DONE =main=
CLOSED: [2022-05-14 Sat 12:08]
#+CAPTION: Wywołanie funkcji
#+INCLUDE: "../../rozne/warsztatowe.c" src c -n 49 :lines "49-56"
